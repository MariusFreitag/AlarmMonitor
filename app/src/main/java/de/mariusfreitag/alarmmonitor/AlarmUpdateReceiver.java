package de.mariusfreitag.alarmmonitor;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

public class AlarmUpdateReceiver extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {
        Util.updateNotification(context);
    }
}
