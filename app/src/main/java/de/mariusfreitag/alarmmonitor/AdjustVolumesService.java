package de.mariusfreitag.alarmmonitor;

import android.app.IntentService;
import android.content.Intent;

public class AdjustVolumesService extends IntentService {

    public AdjustVolumesService() {
        super("AdjustVolumesService");
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        Util.adjustVolumes(getApplicationContext());

        Util.updateNotification(getApplicationContext());
    }
}
