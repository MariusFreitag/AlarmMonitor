package de.mariusfreitag.alarmmonitor;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // Update notification
        Util.updateNotification(this);

        // Start clock app
        startActivity(Util.getStartClockIntent(this));

        // Finish activity
        finish();
    }
}
