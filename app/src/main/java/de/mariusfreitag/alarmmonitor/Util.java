package de.mariusfreitag.alarmmonitor;

import android.app.AlarmManager;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.icu.text.SimpleDateFormat;
import android.icu.util.Calendar;
import android.icu.util.TimeZone;
import android.media.AudioManager;
import android.os.VibrationEffect;
import android.os.Vibrator;

import java.util.Date;

public abstract class Util {

    public static boolean isNotificationPolicyAccessGranted(Context context) {
        NotificationManager notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);

        return notificationManager.isNotificationPolicyAccessGranted();
    }

    public static void adjustVolumes(Context context) {
        Vibrator v = (Vibrator) context.getSystemService(Context.VIBRATOR_SERVICE);

        // If the permission is not granted
        if (!isNotificationPolicyAccessGranted(context)) {
            // Open permission configuration and exit
            context.startActivity(new Intent(android.provider.Settings.ACTION_NOTIFICATION_POLICY_ACCESS_SETTINGS));
            return;
        }

        AudioManager audioManager = (AudioManager) context.getSystemService(Context.AUDIO_SERVICE);
        // Set alarm volume to max
        audioManager.setStreamVolume(AudioManager.STREAM_ALARM, audioManager.getStreamMaxVolume(AudioManager.STREAM_ALARM), 0);
        // Mute all other volume streams
        audioManager.setStreamVolume(AudioManager.STREAM_MUSIC, 0, 0);
        audioManager.setStreamVolume(AudioManager.STREAM_NOTIFICATION, 0, 0);
        audioManager.setStreamVolume(AudioManager.STREAM_RING, 0, 0);
        audioManager.setStreamVolume(AudioManager.STREAM_SYSTEM, 0, 0);

        // Vibrate
        v.vibrate(VibrationEffect.createOneShot(500, VibrationEffect.DEFAULT_AMPLITUDE));
    }

    public static Intent getStartClockIntent(Context context) {
        // Intent to start OnePlus clock app
        return context.getPackageManager().getLaunchIntentForPackage("com.oneplus.deskclock");
    }

    public static PendingIntent getAdjustVolumesServicePendingIntent(Context context) {
        Intent intent = new Intent(context, AdjustVolumesService.class);
        intent.setAction(String.valueOf(System.currentTimeMillis()));

        return PendingIntent.getService(
                context,
                0,
                intent,
                0);
    }

    public static PendingIntent getMainActivityPendingIntent(Context context) {
        Intent intent = new Intent(context, MainActivity.class);
        intent.setAction(String.valueOf(System.currentTimeMillis()));

        return PendingIntent.getActivity(
                context,
                0,
                intent,
                0
        );
    }

    public static void updateNotification(Context context) {
        // Get alarm volume
        AudioManager audioManager = (AudioManager) context.getSystemService(Context.AUDIO_SERVICE);
        int alarmVolume = audioManager.getStreamVolume(AudioManager.STREAM_ALARM);
        int maxAlarmVolume = audioManager.getStreamMaxVolume(AudioManager.STREAM_ALARM);

        // Get next alarm info
        AlarmManager alarmManager = (AlarmManager) context.getSystemService(context.ALARM_SERVICE);
        AlarmManager.AlarmClockInfo nextClock = alarmManager.getNextAlarmClock();

        boolean alarmSet = nextClock != null;
        String nextAlarmTime = "No alarm set";
        String nextAlarmDay = "";

        if (alarmSet) {
            Calendar calendar = Calendar.getInstance();
            calendar.setTimeZone(TimeZone.getTimeZone("UTC"));
            calendar.setTime(new Date(nextClock.getTriggerTime()));

            // Format the alarm time to strings
            SimpleDateFormat formatTime = new SimpleDateFormat("HH:mm");
            SimpleDateFormat formatDay = new SimpleDateFormat("EEE, dd. MMMM yyyy (zz)");
            nextAlarmTime = formatTime.format(calendar.getTime());
            nextAlarmDay = formatDay.format(calendar.getTime());
        }

        // Define notification channels
        NotificationChannel channelAlarmSet = new NotificationChannel("alarm_set", "Alarm set", NotificationManager.IMPORTANCE_DEFAULT);
        NotificationChannel channelAlarmNotSet = new NotificationChannel("alarm_not_set", "Alarm not set", NotificationManager.IMPORTANCE_MIN);

        // Remove lights, vibration and sound of notifications
        channelAlarmSet.enableLights(false);
        channelAlarmSet.enableVibration(false);
        channelAlarmSet.setSound(null, null);
        channelAlarmNotSet.enableLights(false);
        channelAlarmNotSet.enableVibration(false);
        channelAlarmNotSet.setSound(null, null);

        // Create notification channels
        NotificationManager notificationManager = (NotificationManager) context.getSystemService(context.NOTIFICATION_SERVICE);
        notificationManager.createNotificationChannel(channelAlarmSet);
        notificationManager.createNotificationChannel(channelAlarmNotSet);

        // Get intent to open MainActivity on press
        PendingIntent openMainActivityPendingIntent = getMainActivityPendingIntent(context);

        // Get intent to adjust sound profile
        PendingIntent adjustVolumePendingIntent = getAdjustVolumesServicePendingIntent(context);

        // Build notification
        Notification.Builder notificationBuilder =
                new Notification.Builder(context, alarmSet ? channelAlarmSet.getId() : channelAlarmNotSet.getId())
                        .setSmallIcon(alarmSet ? R.drawable.ic_alarm_on_white_24dp : R.drawable.ic_alarm_off_white_24dp)
                        .setContentTitle(nextAlarmTime)
                        .setOngoing(true)
                        .setOnlyAlertOnce(true)
                        .setSubText(alarmVolume == 0 ? "ALARM IS SILENT!" : "Alarm Volume: " + alarmVolume + "/" + maxAlarmVolume)
                        .addAction(new Notification.Action.Builder(null, "ADJUST VOLUMES", adjustVolumePendingIntent).build());

        if (alarmSet) {
            notificationBuilder.setContentText(nextAlarmDay);
        }

        // Set click action to open clock app
        notificationBuilder.setContentIntent(openMainActivityPendingIntent);

        // Cancel notification of other status (alarm set/not set)
        notificationManager.cancel(alarmSet ? 2 : 1);

        // Show notification with id 1 (alarm set) or 2 (alarm not set)
        notificationManager.notify(alarmSet ? 1 : 2, notificationBuilder.build());
    }
}
